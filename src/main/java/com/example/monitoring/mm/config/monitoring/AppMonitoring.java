package com.example.monitoring.mm.config.monitoring;


import com.example.monitoring.mm.config.logging.LoggingAspect;
import com.example.monitoring.mm.config.monitoring.aspects.CountableAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppMonitoring {

    @Bean
    public CountableAspect countableAspect() {
        return new CountableAspect();
    }

    @Bean
    public LoggingAspect loggingAspect() {
        return new LoggingAspect();
    }
}
