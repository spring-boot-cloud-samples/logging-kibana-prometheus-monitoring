package com.example.monitoring.mm.config.logging;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
@Slf4j
public class LoggingAspect {

    @Around("@annotation(loggable)")
    public Object increaseCounter(ProceedingJoinPoint joinPoint, Loggable loggable) throws Throwable {
        final Object[] args = joinPoint.getArgs();
        log.debug("argumets size: " + args.length);
        return joinPoint.proceed(args);
    }
}
