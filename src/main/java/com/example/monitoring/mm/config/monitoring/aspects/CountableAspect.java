package com.example.monitoring.mm.config.monitoring.aspects;

import com.example.monitoring.mm.config.monitoring.tags.Countable;
import io.micrometer.core.instrument.Metrics;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class CountableAspect {
    private static final String ERROR_APPENDER = "-exception";

    @Around("@annotation(countable)")
    public Object increaseCounter(ProceedingJoinPoint joinPoint, Countable countable) throws Throwable {
        final String counterName = countable.value();
        try {
            final Object[] args = joinPoint.getArgs();
            System.out.println("argus: " + args);
            final Object o = joinPoint.proceed(args);
            if (!countable.recordOnlyFailures()) {
                Metrics.counter(counterName).increment();
                System.out.println("Counting values... for: " + counterName);
            }
            return o;
        } catch (Throwable e) {
            Metrics.counter(counterName + ERROR_APPENDER).increment();
            System.out.println("Counting values... for: " + counterName);
            throw e;
        }
    }
}
