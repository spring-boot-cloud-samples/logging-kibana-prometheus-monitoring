package com.example.monitoring.mm.config.monitoring.tags;

import java.lang.annotation.*;

@Inherited
@Target(value = {ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Countable {

    String value() default "method.count";

    String description() default "";

    String[] extraTags() default {};

    boolean recordOnlyFailures() default false;

}
