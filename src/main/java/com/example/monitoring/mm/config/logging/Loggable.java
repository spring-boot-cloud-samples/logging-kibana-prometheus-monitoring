package com.example.monitoring.mm.config.logging;

import java.lang.annotation.*;

@Inherited
@Target(value = {ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Loggable {

    boolean recordTime() default false;

}
