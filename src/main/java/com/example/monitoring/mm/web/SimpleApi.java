package com.example.monitoring.mm.web;

import com.example.monitoring.mm.config.logging.Loggable;
import com.example.monitoring.mm.config.monitoring.tags.Countable;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/sample")
public class SimpleApi {
    private final Counter counter = Metrics.counter("simple-api-counter");
    private final Random random = new Random();

    @Loggable
    @Countable(value = "sample-get")
    @GetMapping
    public Map<String, Object> get(HttpServletRequest request) {
        counter.increment();
        if (random.nextBoolean()) {
            return Map.of("status", "ok", "counterMetric", counter.count(), "path", request.getRequestURL().toString());
        } else {
            throw new RuntimeException("Error fetching from api");
        }
    }
}
