package com.example.monitoring.mm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy
@SpringBootApplication
public class SpringBootMonitoringMicrometerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMonitoringMicrometerApplication.class, args);
    }

}
