
Metric Req/s
```txt
rate(http_server_requests_seconds_count[5m])
```

Metric Req/s Specific Uri
```txt
rate(http_server_requests_seconds_count{uri="/sample", status="200", application="prometheus-demo-spring-boot"}[5m])
```

Metric custom name
```http request
http://localhost:8080/actuator/metrics/simple-api-counter
```
```txt
simple_api_counter_total{application="prometheus-demo-spring-boot"}
```

